import hdwFormat from './index.mjs';
import { require } from './require.mjs';

(async () => {
  const languages = new Set();
  const { languages: supportLanguages } = await hdwFormat.getSupportInfo();

  for (const { vscodeLanguageIds } of supportLanguages) {
    for (const vscodeLanguageId of vscodeLanguageIds || []) {
      languages.add(vscodeLanguageId);
    }
  }

  console.log(
    JSON.stringify({
      languages: Array.from(languages),
      path: require.resolve('./index.mjs'),
    }),
  );
})();
