import ignore from 'ignore';
import { accessSync, readFileSync } from 'node:fs';
import { resolve } from 'node:path';
import picomatch from 'picomatch';
import prettierrc from './.prettierrc.mjs';
import { require } from './require.mjs';

const { plugins: externalPlugins } = prettierrc;
const configPath = require.resolve('./.prettierrc.mjs');

async function resolveCheckSupport(cwd) {
  const prettierIgnorePath = resolve(cwd, '.prettierignore');
  const prettierIgnore = ignore({ allowRelativePaths: true });

  prettierIgnore.add('.git', '.hg', '.svn', 'node_modules');

  try {
    accessSync(prettierIgnorePath, R_OK | W_OK);
    prettierIgnore.add(readFileSync(prettierIgnorePath, 'utf8'));
  } catch {}

  // File Extensions
  const { languages } = await this.getSupportInfo();
  const extensions = languages
    .flatMap(lang => lang.extensions || [])
    .map(extension => `*${extension[0] !== '.' ? '.' : ''}${extension}`);

  const filenames = languages.flatMap(lang => lang.filenames || []);
  const isMatch = picomatch(`**/{${[...extensions, ...filenames]}}`, {
    basename: true,
    dot: true,
    posixSlashes: true,
  });

  return path => {
    return !prettierIgnore.ignores(path) && isMatch(path);
  };
}

function wrap(fn, optionsArgumentIndex) {
  return (...args) => {
    if (optionsArgumentIndex in args) {
      args[optionsArgumentIndex].plugins = externalPlugins;
    } else {
      args[optionsArgumentIndex] = {
        plugins: externalPlugins,
      };
    }

    return fn(...args);
  };
}

const apiList = ['formatWithCursor', 'getFileInfo', 'getSupportInfo'];

const debugList = ['parse', 'formatAST', 'formatDoc', 'printToDoc', 'printDocToString'];

export default function (prettier) {
  const { __debug, resolveConfig } = prettier;
  const hdwFormat = Object.setPrototypeOf({ __debug: {} }, prettier);

  const newResolveConfig = function (filePath, opts) {
    if (!opts) {
      opts = {};
    }

    opts.config = configPath;

    return resolveConfig.call(this, filePath, opts);
  };

  hdwFormat.resolveConfig = newResolveConfig;
  hdwFormat.resolveCheckSupport = resolveCheckSupport;

  for (const name of apiList) {
    hdwFormat[name] = wrap(prettier[name], name === 'getSupportInfo' ? 0 : 1);
  }

  for (const name of debugList) {
    hdwFormat.__debug[name] = wrap(__debug[name], hdwFormat.__debug, 1);
  }

  return hdwFormat;
}
