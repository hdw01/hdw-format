import { readFileSync, writeFileSync } from 'node:fs';
import hdwFormat from './index.mjs';

const { stdin, stdout } = process;

(async () => {
  const checkSupport = await hdwFormat.resolveCheckSupport(process.cwd());

  function encode(dataTransfer) {
    return dataTransfer.type;
  }

  function decode(raw) {
    return { type: raw.trim() };
  }

  stdin.on('readable', async () => {
    let json = '';
    let chunk;

    while ((chunk = stdin.read())) {
      json += chunk;
    }

    const { type } = decode(json);

    if (type === 'error') {
      return;
    } else if (checkSupport(type)) {
      try {
        const options = await hdwFormat.resolveConfig(type);

        writeFileSync(type, hdwFormat.format(readFileSync(type, 'utf8'), options));

        stdout.write(`${encode({ type })}\n`);
      } catch (error) {
        stdout.write(`Error: ${`${error}`.replace('\n', ' ')}\n`);
      }
    }
  });
})();
