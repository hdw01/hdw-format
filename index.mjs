import * as prettier from 'prettier';
import wrapper from './wrapper.mjs';

export default wrapper(prettier);
