import java.io.StringReader
import java.nio.file.Paths
import java.nio.charset.Charset

import com.intellij.AppTopics
import com.intellij.execution.configurations.GeneralCommandLine
import com.intellij.execution.process.OSProcessHandler
import com.intellij.execution.process.ProcessEvent
import com.intellij.execution.process.ProcessListener
import com.intellij.openapi.Disposable
import com.intellij.openapi.editor.Document
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.fileEditor.FileDocumentManagerListener
import com.intellij.openapi.util.Key
import com.intellij.openapi.util.SystemInfo
import com.intellij.openapi.vfs.VirtualFileManager
import com.intellij.util.application

import liveplugin.*

data class DataTransfer(val type: String)

class WatcherProcess : OSProcessHandler(GeneralCommandLine("hdw-format" + (if (SystemInfo.isWindows) ".cmd" else ""), "--stdin")), ProcessListener {

  private val writer = processInput.writer(Charset.defaultCharset())

  init {
    addProcessListener(this)
    startNotify()
  }

  private fun encode(dataTransfer: DataTransfer): String {
    return dataTransfer.type
  }

  private fun decode(raw: String): DataTransfer {
    return DataTransfer(raw.trim())
  }

  fun emit(dataTransfer: DataTransfer) {
    StringReader(encode(dataTransfer)).transferTo(writer)

    writer.flush()
  }

  override fun processTerminated(event: ProcessEvent) {
    show("Watcher dispose.")
  }

  override fun startNotified(event: ProcessEvent) {
    show("Watcher started.")
  }

  override fun onTextAvailable(event: ProcessEvent, key: Key<*>) {
    runLaterOnEdt {
      val dataTransfer = decode(event.text)

      if (dataTransfer.type.isNotBlank() && !dataTransfer.type.startsWith("Error:")) {
        val virtualFile = VirtualFileManager.getInstance().findFileByNioPath(Paths.get(dataTransfer.type))

        virtualFile?.refresh(false, false)
      } else {
        show(dataTransfer.type)
      }
    }
  }

}

class WatcherProjectListener : Disposable, FileDocumentManagerListener {

  private val process = WatcherProcess()

  override fun beforeDocumentSaving(document: Document) {
    runLaterOnEdt {
      val virtualFile = FileDocumentManager.getInstance().getFile(document)

      if(virtualFile?.path != null) {
        process.emit(DataTransfer(virtualFile.path))
      }
    }
  }

  override fun dispose() {
    process.destroyProcess()
  }
}

val watcher = WatcherProjectListener()
val observer = application.messageBus.connect(watcher)

observer.subscribe(AppTopics.FILE_DOCUMENT_SYNC, watcher)

pluginDisposable.whenDisposed {
  watcher.dispose()
  observer.dispose()
}