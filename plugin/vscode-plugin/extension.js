const vscode = require('vscode');
const { exec } = require('child_process');
const { Worker } = require('worker_threads');
const { resolve } = require('path');

class FormatterWorker {
  constructor(path) {
    const tasks = new Map();
    const worker = new Worker(resolve(__dirname, 'worker.js'), {
      workerData: path,
    });

    worker.on('message', ({ id, result, error }) => {
      if (tasks.has(id)) {
        const [resolve, reject] = tasks.get(id);

        if (error) {
          reject(error);
        } else {
          resolve(result);
        }

        tasks.delete(id);
      }
    });

    this.disposable = new vscode.Disposable(() => {
      const { tasks, worker } = this;

      for (const [_, reject] of tasks) {
        reject('terminated.');
      }

      tasks.clear();
      worker.terminate();
    });

    this.worker = worker;
    this.tasks = tasks;
  }

  support(fsPath, workspacePath) {
    return new Promise((resolve, reject) => {
      const { tasks, worker } = this;
      const id = Date.now().toString(36);

      tasks.set(id, [resolve, reject]);

      worker.postMessage({ id, type: 'support', args: [fsPath, workspacePath] });
    });
  }

  format(fsPath, textContent) {
    return new Promise((resolve, reject) => {
      const { tasks, worker } = this;
      const id = Date.now().toString(36);

      tasks.set(id, [resolve, reject]);

      worker.postMessage({ id, type: 'format', args: [fsPath, textContent] });
    });
  }
}

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
  exec('hdw-format --vsx', async (error, stdout, stderr) => {
    if (error) {
      vscode.window.showErrorMessage(`${error}`);

      return;
    }

    try {
      const { languages, path } = JSON.parse(stdout);
      const formatter = new FormatterWorker(path);

      const disposable = vscode.languages.registerDocumentFormattingEditProvider(languages, {
        async provideDocumentFormattingEdits(document, options, token) {
          const { uri } = document;
          const { fsPath } = uri;
          const workspacePath = vscode.workspace.getWorkspaceFolder(uri)?.uri.fsPath || '_';
          const result = [];

          if (await formatter.support(fsPath, workspacePath)) {
            const range = new vscode.Range(
              document.lineAt(0).range.start,
              document.lineAt(document.lineCount - 1).rangeIncludingLineBreak.end,
            );

            try {
              const formatted = await formatter.format(fsPath, document.getText(range));

              result.push(new vscode.TextEdit(range, formatted));
            } catch (error) {}
          }

          return result;
        },
      });

      context.subscriptions.push(formatter.disposable, disposable);

      vscode.window.showInformationMessage('Watcher started.');
    } catch (error) {
      vscode.window.showErrorMessage(`${error}`);
    }
  });
}

function deactivate() {}

module.exports = {
  activate,
  deactivate,
};
