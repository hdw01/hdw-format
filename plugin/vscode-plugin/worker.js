const { parentPort, workerData } = require('worker_threads');

const module$ = import(workerData);
const supportMap = new Map();

parentPort.on('message', async ({ id, type, args }) => {
  try {
    const { default: hdwFormat } = await module$;

    switch (type) {
      case 'support':
        if (supportMap.has(args[0])) {
          parentPort.postMessage(supportMap.get(args[1])(args[0]));
        } else {
          const checkSupport = await hdwFormat.resolveCheckSupport(args[1]);

          supportMap.set(args[1], checkSupport);

          parentPort.postMessage({ id, result: checkSupport(args[0]), error: null });
        }
        break;
      case 'format':
        const formatOptions = await hdwFormat.resolveConfig(args[0]);
        const formatted = await hdwFormat.format(args[1], formatOptions);

        parentPort.postMessage({ id, result: formatted, error: null });
        break;
    }
  } catch (error) {
    parentPort.postMessage({ id, result: null, error: `${error}` });
  }
});
