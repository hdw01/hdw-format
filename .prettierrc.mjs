import pluginXml from '@prettier/plugin-xml';
import pluginCssOrder from 'prettier-plugin-css-order';
import pluginJava from 'prettier-plugin-java';
import pluginOrganizeAttributes from 'prettier-plugin-organize-attributes';
import pluginOrganizeImports from 'prettier-plugin-organize-imports';
import pluginSh from 'prettier-plugin-sh';

export default {
  plugins: [pluginXml, pluginCssOrder, pluginJava, pluginOrganizeAttributes, pluginOrganizeImports, pluginSh],
  overrides: [
    {
      files: ['*.html'],
      options: {
        parser: 'angular',
        printWidth: 140,
        singleAttributePerLine: true,
        singleQuote: true,
        htmlWhitespaceSensitivity: 'css',
        attributeGroups: [
          '$ANGULAR_STRUCTURAL_DIRECTIVE',
          '$ANGULAR_ELEMENT_REF',
          '$ANGULAR_TWO_WAY_BINDING',
          '$ANGULAR_OUTPUT',
          '$ANGULAR_INPUT',
          '$ANGULAR_ANIMATION_INPUT',
          '$ANGULAR_ANIMATION',
          '$ID',
          '$NAME',
        ],
        attributeSort: 'ASC',
      },
    },
    {
      files: ['*.mjs', '*.js', '*.jsx', '*.ts', '*.tsx'],
      options: {
        parser: 'babel-ts',
        semi: true,
        singleQuote: true,
        printWidth: 120,
        arrowParens: 'avoid',
        bracketSameLine: true,
      },
    },
    {
      files: ['*.java'],
      options: {
        parser: 'java',
        semi: true,
        printWidth: 140,
      },
    },
    {
      files: ['*.json'],
      options: {
        parser: 'json',
      },
    },
    {
      files: ['*.xml'],
      options: {
        parser: 'xml',
        printWidth: 140,
        xmlWhitespaceSensitivity: 'ignore',
      },
    },
    {
      files: ['*.css'],
      options: {
        parser: 'css',
        order: 'smacss',
        keepOverrides: false,
      },
    },
    {
      files: ['*.scss'],
      options: {
        parser: 'scss',
        order: 'smacss',
        keepOverrides: false,
      },
    },
    {
      files: ['.env', '.gitignore', 'Dockerfile', '*.sh'],
      options: {
        parser: 'sh',
      },
    },
    {
      files: ['*.md'],
      options: {
        parser: 'markdown',
      },
    },
    {
      files: ['*.mdx'],
      options: {
        parser: 'mdx',
      },
    },
    {
      files: ['*.yml', '*.yaml'],
      options: {
        parser: 'yaml',
      },
    },
  ],
};
