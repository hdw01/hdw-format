#!/usr/bin/env node

import { run } from 'prettier/internal/cli.mjs';
import { require } from '../require.mjs';

if (process.argv.indexOf('--stdin') !== -1) {
  import('../stdin.mjs');
} else if (process.argv.indexOf('--vsx') !== -1) {
  import('../vsx.mjs');
} else {
  const args = ['--config', require.resolve('./.prettierrc.mjs'), ...process.argv.slice(2)];

  run(args);
}
